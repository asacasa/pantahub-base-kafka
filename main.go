package main

import (
	"fmt"
	"os"

	"gitlab.com/pantacor/pantahub-listen/controllers"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Controller name is required. eg: <build> <steps|devices> [<non-default-topic>] [<non-default-consumer-group>]")
		return
	}
	controllerName := os.Args[1]

	topic := os.Getenv("KAFKA_TOPIC")
	if len(os.Args) == 3 {
		topic = os.Args[2]
	}

	consumerGroup := os.Getenv("KAFKA_CONSUMER_GROUP")
	if len(os.Args) == 4 {
		consumerGroup = os.Args[3]
	}

	if controllerName == "devices" {
		ns := controllers.NewDeviceProcessor(topic, consumerGroup)
		ns.Run()
	} else if controllerName == "steps" {
		ns := controllers.NewStepProcessor(topic, consumerGroup)
		ns.Run()
	} else {
		fmt.Println("Invalid controller(Note:Use devices or steps,syntax:<build> <controllername> [<non-default-topic>] [<non-default-consumer-group>])")
	}
}
