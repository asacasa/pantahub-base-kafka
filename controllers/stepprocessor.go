package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/segmentio/kafka-go"
	_ "github.com/segmentio/kafka-go/snappy"
	"gitlab.com/pantacor/pantahub-base/utils"
	"gopkg.in/resty.v1"
)

var KafkaStepConsumerGroup = "consumer-group-objcontrol-steps"

var KafkaStepTopic = "pantabasemgo.pantabase-serv.pantahub_steps"

type StepProcessor struct{}

func (s *StepProcessor) HandleMessage(m kafka.Message) error {

	fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

	fmt.Printf("\n\nReceived a change in topic:%s", m.Topic)
	var step interface{}
	err := json.Unmarshal([]byte(m.Value), &step)
	if err != nil {
		fmt.Print(err)
	}
	StepMap := map[string]interface{}{}
	err = json.Unmarshal([]byte(step.(string)), &StepMap)
	if err != nil {
		fmt.Print(err)
	}
	stepID, ok := StepMap["_id"].(string)
	if ok {
		fmt.Printf("\nStep id:%s\n", stepID)
		response, res := ChangeStepCallback(stepID)
		log.Println("Response:")
		_, ok := response["step_id"].(string)
		if ok {
			PrettyPrint(response)
		} else {
			PrettyPrint(res)
		}
	}

	return nil
}

// ChangeStepCallback : Change Step Callback
func ChangeStepCallback(stepID string) (map[string]interface{}, *resty.Response) {
	response := map[string]interface{}{}

	BaseAPIURL := GetBaseURL()

	APIEndPoint := BaseAPIURL + "/callbacks/steps/" + stepID

	log.Print("Calling PUT " + APIEndPoint)
	request := resty.R()
	request.SetBasicAuth("saadmin", utils.GetEnv(utils.EnvPantahubSaAdminSecret))
	res, err := request.Put(APIEndPoint)
	if err != nil {
		fmt.Print(err.Error())
	}

	err = json.Unmarshal(res.Body(), &response)
	if err != nil {
		fmt.Print(err.Error())
	}
	return response, res
}

func (s *StepProcessor) Run() {
	l := log.New(os.Stdout, "kafka-logger: ", 0)
	e := log.New(os.Stdout, "kafka-error: ", 0)
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:         []string{GetKafkaURL()},
		GroupID:         KafkaStepConsumerGroup,
		Topic:           KafkaStepTopic,
		MinBytes:        10e3, // 10KB
		MaxBytes:        10e6, // 10MB
		Logger:          kafka.LoggerFunc(l.Printf),
		ErrorLogger:     kafka.LoggerFunc(e.Printf),
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	})

	for {
		m, err := r.ReadMessage(context.Background())
		if err != nil {
			break
		}
		s.HandleMessage(m)
	}
	r.Close()
}

func NewStepProcessor(topic, consumerGroup string) KafkaTopicController {
	if topic != "" {
		KafkaStepTopic = topic
	}
	if consumerGroup != "" {
		KafkaStepConsumerGroup = consumerGroup
	}
	return &StepProcessor{}
}
