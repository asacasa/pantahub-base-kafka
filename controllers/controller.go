package controllers

import (
	"encoding/json"
	"fmt"

	"github.com/segmentio/kafka-go"
	"gitlab.com/pantacor/pantahub-base/utils"
)

type KafkaTopicController interface {
	HandleMessage(msg kafka.Message) error
	Run()
}

// GetBaseURL : Get Base URL
func GetBaseURL() string {
	scheme := utils.GetEnv(utils.EnvPantahubScheme)
	host := utils.GetEnv(utils.EnvPantahubHost)
	port := utils.GetEnv(utils.EnvPantahubPort)
	BaseAPIURL := scheme + "://" + host + ":" + port
	return BaseAPIURL
}

// GetKafkaURL : Get Kafka URL
func GetKafkaURL() string {
	host := utils.GetEnv(utils.EnvKafkaHost)
	port := utils.GetEnv(utils.EnvKafkaPort)
	KafkaURL := host + ":" + port
	return KafkaURL
}

// PrettyPrint : Pretty Print
func PrettyPrint(value interface{}) {
	b, err := json.MarshalIndent(value, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(b))
}
